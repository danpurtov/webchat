﻿using WebChat.ChatEntities;

namespace WebChat.UsersEntities
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; } = ""; // имя пользователя
        public string Email { get; set; } = ""; // возраст пользователя

        public string Password { get; set; } = "";
        public List<Message> SendedMessages { get; set; } = new List<Message>();
        public List<Message> ReceivedMessages { get; set; } = new List<Message>();
    }
}

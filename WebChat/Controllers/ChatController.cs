﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebChat.ChatEntities;
using WebChat.DB_Context;

namespace WebChat.Controllers
{
    public class ChatController : Controller
    {
        private readonly ChatRepository chatRepository;
        //храним дату и время
        public DateTime startDateTime;
        public ChatController(ChatRepository chatRepository)
        {
            startDateTime = DateTime.Now;
            this.chatRepository = chatRepository;
            
        }

        // GET: DialogController
        public ActionResult Dialog()
        {
            ViewBag.Messages = new List<string>();
            var messages = chatRepository.GetMessages();
            foreach (var message in messages)
            {
                ViewBag.Messages.Add(message.Value);
            }
            //var timer = new Timer(UpdateView, null, 0, 2000);
            return View();
        }
        public void UpdateView(string message)
        {
            ViewBag.Messages = new List<string>();
            //var messages = chatRepository.GetMessages();
            //foreach (var message in messages)
            //{
            //    ViewBag.Messages.Add(message.Value);
            //}
        }

    }
}

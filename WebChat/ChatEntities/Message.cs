﻿using System.ComponentModel.DataAnnotations.Schema;
using WebChat.UsersEntities;

namespace WebChat.ChatEntities
{
    public class Message
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public DateTime DateTime { get; set; } = DateTime.Now;
        public bool isDeleted { get; set; } = false;

        public int SenderId { get; set; }
        [ForeignKey("SenderId")]
        public virtual User Sender { get; set; }
        public int ReceiverId { get; set; }
        [ForeignKey("ReceiverId")]
        public virtual User Receiver { get; set; }

    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using WebChat.UsersEntities;

namespace WebChat.ChatEntities
{
    public class Dialog
    {
        public int Id { get; set; }
        public int SenderId { get; set; }
        [ForeignKey("SenderId")]
        public virtual User Sender { get; set; }
        public int ReceiverId { get; set; }
        [ForeignKey("ReceiverId")]
        public virtual User Receiver { get; set; }

    }
}

﻿using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;
using WebChat.ChatEntities;
using WebChat.UsersEntities;

namespace WebChat.DB_Context
{
    public class ApplicationContext: DbContext
    {
        public DbSet<User> Users { get; set; } = null!;
        public DbSet<Message> Messages { get; set; } = null!;
        //public DbSet<Dialog> Dialogs { get; set; } = null!; 
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();   // создаем базу данных при первом обращении
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Message>()
                .HasOne(u => u.Sender)
                .WithMany(m => m.SendedMessages)
                .HasForeignKey(u => u.SenderId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Message>()
                .HasOne(u => u.Receiver)
                .WithMany(m => m.ReceivedMessages)
                .HasForeignKey(u => u.ReceiverId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<User>().HasData(
                    new User { Id = 1, Name = "Tom", Email = "user1@mail.ru", Password = "12345" },
                    new User { Id = 2, Name = "Bob", Email = "user2@mail.ru", Password = "12345" }
            );
            modelBuilder.Entity<Message>().HasData(
                new Message { Id = 1, Value = "Hi! It`s our first message.", SenderId=1, ReceiverId=2, DateTime=DateTime.Now, isDeleted=false/*, DialogId = 1, UserId = 1 */}
                ) ;
            //modelBuilder.Entity<Dialog>().HasData(
            //    new Dialog { Id = 1, SenderId = 1, ReceiverId = 2}
            //    );
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using WebChat.ChatEntities;
using WebChat.DB_Context;
using WebChat.UsersEntities;

namespace WebChat
{
    public class ChatRepository
    {
        private readonly ApplicationContext context;
        public ChatRepository(ApplicationContext context)
        {
            this.context = context;
        }
        public IQueryable<User> GetUsers()
        {
            return context.Users.OrderBy(x => x.Name);
        }
        public List<Message> GetMessages()
        {
            return context.Messages.Where(m=>m.SenderId==1&&m.ReceiverId==2).ToList();
        }
        public List<Message> GetMessageAfter(DateTime dateTime)
        {
            var messages = context.Messages.Where(m => m.SenderId == 1 && m.ReceiverId == 2 && m.DateTime >= dateTime).ToList();
            return messages;
        }
        public User GetArticleById(int id)
        {
            return context.Users.Single(x => x.Id == id);
        }
        //сохранить новую либо обновить существующую запись в БД
        public int SaveArticle(User entity)
        {
            if (entity.Id == default)
                context.Entry(entity).State = EntityState.Added;
            else
                context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

            return entity.Id;
        }

        //удалить существующую запись
        public void DeleteArticle(User entity)
        {
            context.Users.Remove(entity);
            context.SaveChanges();
        }
    }
}
